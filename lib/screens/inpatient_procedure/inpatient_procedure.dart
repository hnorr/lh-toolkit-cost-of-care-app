import 'package:cost_of_care/screens/inpatient_procedure/search_inpatient.dart';
import 'package:flutter/material.dart';

import 'package:cost_of_care/screens/inpatient_procedure/components/body.dart';

class InpatientProcedureScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inpatient Procedures'),
        centerTitle: true,
        backgroundColor: Colors.orange,
        leading: BackButton(color: Colors.white),
        actions: [
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              SearchInpatient();
              showSearch(
                context: context,
                delegate: SearchInpatient(),
              );
            },
          )
        ],
      ),
      body: Body(),
    );
  }
}
